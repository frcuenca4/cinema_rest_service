package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.dto.movie_announcement.MovieAnnouncementDto;
import com.ganiev.cinemarestservice.service.movie_announcement.MovieAnnouncementServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/movie-announcement")
public class MovieAnnouncementController {

    private final MovieAnnouncementServiceImpl movieAnnouncementService;

    @PostMapping
    public HttpEntity<?> addNewAnnouncement(@RequestBody MovieAnnouncementDto dto) {
        return movieAnnouncementService.addNewAnnouncement(dto);
    }

    @GetMapping
    public HttpEntity<?> getAllAnnouncements(){
        return movieAnnouncementService.getAllAnnouncements();
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteAnnouncement(@PathVariable UUID id){
        return movieAnnouncementService.deleteAnnouncement(id);
    }
}
