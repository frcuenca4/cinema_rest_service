package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.dto.hall.HallDto;
import com.ganiev.cinemarestservice.service.hall.HallServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/hall")
public class HallController {

    private final HallServiceImpl hallService;

    @Autowired
    public HallController(HallServiceImpl hallService) {
        this.hallService = hallService;
    }

    @PostMapping({"", "/{id}"})
    public HttpEntity<?> saveHall(@PathVariable(required = false) UUID id,
                                  @RequestBody HallDto dto) {
        return hallService.saveHall(id, dto);
    }


    @DeleteMapping(path = "/{id}")
    public HttpEntity<?> deleteHall(@PathVariable UUID id) {
        return hallService.deleteHall(id);
    }
}
