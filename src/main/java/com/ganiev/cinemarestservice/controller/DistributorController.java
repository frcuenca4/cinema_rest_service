package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.dto.distributor.DistributorDto;
import com.ganiev.cinemarestservice.service.distributor.DistributorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/distributor")
public class DistributorController {

    private final DistributorServiceImpl distributorService;

    @Autowired
    public DistributorController(DistributorServiceImpl distributorService) {
        this.distributorService = distributorService;
    }

    @PostMapping
    public HttpEntity<?> createNewDistributor(DistributorDto distributorDto) {
        return distributorService.createNewDistributor(distributorDto);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getDistributorById(@PathVariable UUID id) {
        return distributorService.getDistributorById(id);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editDistributor(DistributorDto dto, @PathVariable UUID id) {
        return distributorService.editDistributor(id, dto);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteDistributor(@PathVariable UUID id) {
        return distributorService.deleteDistributor(id);
    }
}
