package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.service.attachment.AttachmentService;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequestMapping("api/attachment")
public class AttachmentController {

    private final
    AttachmentService attachmentService;

    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @PostMapping
    public HttpEntity<?> saveAttachment(@RequestParam("file") MultipartFile file) {
        return attachmentService.saveAttachment(file);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getAttachmentById(@PathVariable UUID id) {
        return attachmentService.getAttachmentById(id);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteAttachment(@PathVariable UUID id){
        return attachmentService.delete(id);
    }
}
