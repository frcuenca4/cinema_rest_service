package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.dto.cast.CastDto;
import com.ganiev.cinemarestservice.service.cast.CastServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/cast")
public class CastController {

    private final CastServiceImpl castService;

    @Autowired
    public CastController(CastServiceImpl castService) {
        this.castService = castService;
    }

    @PostMapping({"", "/{id}"})
    public HttpEntity<?> saveCast(@PathVariable(required = false) UUID id,
                                  @ModelAttribute CastDto dto) {
        return castService.saveCast(id, dto);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCastById(@PathVariable UUID id) {
        return castService.getCastById(id);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCats(@PathVariable UUID id) {
        return castService.deleteCast(id);
    }

}
