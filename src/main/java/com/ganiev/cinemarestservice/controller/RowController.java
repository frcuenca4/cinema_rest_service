package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.dto.row.RowDto;
import com.ganiev.cinemarestservice.service.row.RowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/row")
public class RowController {

    private final RowService rowService;

    @Autowired
    public RowController(RowService rowService) {
        this.rowService = rowService;
    }

    @PostMapping
    public HttpEntity<?> saveRow(@RequestBody RowDto dto) {
        return rowService.saveRowSeats(dto);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteRow(@PathVariable UUID id) {
        return rowService.deleteRow(id);
    }
}
