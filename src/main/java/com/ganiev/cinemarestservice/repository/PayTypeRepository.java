package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.PayType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "pay_type", path = "pay_type")
public interface PayTypeRepository extends JpaRepository<PayType, UUID> {
}
