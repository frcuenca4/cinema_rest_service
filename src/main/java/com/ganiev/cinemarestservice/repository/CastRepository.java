package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.Cast;
import com.ganiev.cinemarestservice.projection.cast.CastView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface CastRepository extends JpaRepository<Cast, UUID> {

    CastView getCastById(UUID id);
}
