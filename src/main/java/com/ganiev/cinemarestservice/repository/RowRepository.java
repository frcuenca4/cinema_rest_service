package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.Row;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface RowRepository extends JpaRepository<Row, UUID> {

    @Query(
            value = "select cast(r.id as varchar) as id from row r where hall_id = :uuid",
            nativeQuery = true)
    List<UUID> findRowByHallId(UUID uuid);
}
