package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.Distributor;
import com.ganiev.cinemarestservice.projection.distributor.DistributorView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DistributorRepository extends JpaRepository<Distributor, UUID> {

    @Query(
            value = "select d.id as id, d.name as name, d.description as description, d.logo.id as logoId from Distributor d where d.id = :uuid"
    )
    DistributorView getDistributorById(@Param("uuid") UUID uuid);
}
