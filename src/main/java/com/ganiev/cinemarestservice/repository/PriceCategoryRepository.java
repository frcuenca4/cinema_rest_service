package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.PriceCategory;
import com.ganiev.cinemarestservice.projection.price_category.PriceCategoryView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(
        collectionResourceRel = "price-category",
        path = "price-category",
        excerptProjection = PriceCategoryView.class)
public interface PriceCategoryRepository extends JpaRepository<PriceCategory, UUID> {

}
