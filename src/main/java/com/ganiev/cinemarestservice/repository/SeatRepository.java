package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.Seat;
import com.ganiev.cinemarestservice.projection.seat.AvailableSeatView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface SeatRepository extends JpaRepository<Seat, UUID> {
    @Query(value = "select cast(id as varchar) as id from seat where row_id = :id",
            nativeQuery = true)
    List<UUID> findSeatByRowId(UUID id);


    @Query(nativeQuery = true,
            value = "select cast(s.id as varchar) as id,\n" +
                    "       s.number              as seatNumber,\n" +
                    "       r.number              as rowNumber,\n" +
                    "       true                  as isAvailable\n" +
                    "from seat s\n" +
                    "         join row r on r.id = s.row_id\n" +
                    "         join hall h on h.id = r.hall_id\n" +
                    "         join movie_session ms on h.id = ms.hall_id\n" +
                    "where s.id not in (\n" +
                    "    select t.seat_id\n" +
                    "    from ticket t\n" +
                    "             join movie_session ms on ms.id = t.movie_session_id\n" +
                    "    where t.movie_session_id = :sessionId\n" +
                    "      and t.status <> 'STATUS_REFUNDED'\n" +
                    ")\n" +
                    "  and ms.id = :sessionId\n" +
                    "union\n" +
                    "select cast(s.id as varchar) as id,\n" +
                    "       s.number              as seatNumber,\n" +
                    "       r.number              as rowNumber,\n" +
                    "       false                 as isAvailable\n" +
                    "from seat s\n" +
                    "         join row r on r.id = s.row_id\n" +
                    "         join hall h on h.id = r.hall_id\n" +
                    "         join movie_session ms on h.id = ms.hall_id\n" +
                    "         join ticket t2 on s.id = t2.seat_id\n" +
                    "where t2.movie_session_id = :sessionId\n" +
                    "  and t2.status <> 'STATUS_REFUNDED'\n" +
                    "order by rowNumber, seatNumber;\n")
    List<AvailableSeatView> getAvailableSeats(UUID sessionId);
}
