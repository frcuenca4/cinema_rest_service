package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.Hall;
import com.ganiev.cinemarestservice.projection.movie_session.SessionHallDateView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface HallRepository extends JpaRepository<Hall, UUID> {
    Optional<Hall> findByName(String name);
}
