package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.PurchaseWaitingTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(
        collectionResourceRel = "purchase_waiting_time",
        path = "purchase_waiting_time")
public interface PurchaseWaitingTimeRepository extends JpaRepository<PurchaseWaitingTime, UUID> {
}
