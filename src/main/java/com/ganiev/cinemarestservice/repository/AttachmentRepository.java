package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {

}
