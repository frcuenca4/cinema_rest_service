package com.ganiev.cinemarestservice.dto.ticket;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TicketDto {

    UUID sessionId;

    UUID seatId;
}
