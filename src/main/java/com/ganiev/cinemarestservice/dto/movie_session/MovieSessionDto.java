package com.ganiev.cinemarestservice.dto.movie_session;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class MovieSessionDto {
    UUID announcementId;

    List<ReserveHallDto> reservedHalls;

}
