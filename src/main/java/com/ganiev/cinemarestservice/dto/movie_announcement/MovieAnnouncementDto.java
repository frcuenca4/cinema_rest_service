package com.ganiev.cinemarestservice.dto.movie_announcement;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class MovieAnnouncementDto {
    UUID movieId;

    String AnnouncementStatus;
}
