package com.ganiev.cinemarestservice.service.cast;

import com.ganiev.cinemarestservice.dto.cast.CastDto;
import com.ganiev.cinemarestservice.entity.Attachment;
import com.ganiev.cinemarestservice.entity.Cast;
import com.ganiev.cinemarestservice.entity.enums.CastType;
import com.ganiev.cinemarestservice.projection.cast.CastView;
import com.ganiev.cinemarestservice.repository.AttachmentRepository;
import com.ganiev.cinemarestservice.repository.CastRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
public class CastServiceImpl implements CastService {

    private final CastRepository castRepo;
    private final AttachmentRepository attachmentRepo;

    @Autowired
    public CastServiceImpl(CastRepository castRepo, AttachmentRepository attachmentRepo) {
        this.castRepo = castRepo;
        this.attachmentRepo = attachmentRepo;
    }

    @Override
    public HttpEntity<?> saveCast(UUID id, CastDto dto) {
        if (id != null) {
            Optional<Cast> optionalCast = castRepo.findById(id);

            if (optionalCast.isPresent()) {
                Cast cast = optionalCast.get();
                if (dto.getImage() != null) {
                    try {
                        attachmentRepo.deleteById(cast.getPhoto().getId());

                        cast.setFullName(dto.getFullName());
                        cast.setCastType(CastType.valueOf(dto.getCastType()));
                        cast.setPhoto(Attachment.prepareAttachment(dto.getImage()));
                        castRepo.save(cast);

                        return ResponseEntity.ok(SUCCESS_SAVE);
                    } catch (IOException e) {
                        return ResponseEntity.status(HttpStatus.CONFLICT).body(FAILED_TO_SAVE);
                    }
                } else {
                    cast.setFullName(dto.getFullName());
                    cast.setCastType(CastType.valueOf(dto.getCastType()));
                    castRepo.save(cast);
                    return ResponseEntity.ok(SUCCESS_SAVE);
                }
            } else
                return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
        } else {
            try {
                Cast cast = new Cast(
                        dto.getFullName(),
                        CastType.valueOf(dto.getCastType()),
                        Attachment.prepareAttachment(dto.getImage())
                );
                castRepo.save(cast);
                return ResponseEntity.ok(SUCCESS_SAVE);
            } catch (IOException e) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(FAILED_TO_SAVE);
            }
        }
    }

    @Override
    public HttpEntity<?> getCastById(UUID id) {
        CastView castView = castRepo.getCastById(id);
        if (castView.getId() != null) {
            return ResponseEntity.ok(castView);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }

    @Override
    public HttpEntity<?> deleteCast(UUID id) {
        Optional<Cast> optionalCast = castRepo.findById(id);
        if (optionalCast.isPresent()) {
            attachmentRepo.deleteById(optionalCast.get().getPhoto().getId());
            castRepo.delete(optionalCast.get());
            return ResponseEntity.ok(SUCCESS_DELETE);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }
}
