package com.ganiev.cinemarestservice.service.movie;

import com.ganiev.cinemarestservice.dto.movie.MovieDto;
import org.springframework.http.HttpEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface MovieService {
    public HttpEntity<?> getMovies(Integer size,
                                   Integer page,
                                   String search,
                                   Boolean direction,
                                   String sort);

    public HttpEntity<?> getMovieById(UUID id);

    public HttpEntity<?> saveMovie(UUID id, MultipartFile file, MovieDto dto);

    public HttpEntity<?> deleteMovie(UUID id);
}
