package com.ganiev.cinemarestservice.service.movie;

import com.ganiev.cinemarestservice.dto.movie.MovieDto;
import com.ganiev.cinemarestservice.entity.*;
import com.ganiev.cinemarestservice.projection.movie.MovieView;
import com.ganiev.cinemarestservice.projection.movie.SingleMovieView;
import com.ganiev.cinemarestservice.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
@RequiredArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepo;
    private final DistributorRepository distributorRepo;
    private final CastRepository castRepo;
    private final CountryRepository countryRepo;
    private final GenreRepository genreRepo;

    @Override
    public HttpEntity<?> getMovies(Integer size, Integer page, String search, Boolean direction, String sort) {

        try {
            Pageable pageable = PageRequest.of(
                    page - 1,
                    size,
                    direction ? Sort.Direction.ASC : Sort.Direction.DESC,
                    sort
            );

            List<MovieView> allMovies = movieRepo.findAllMovies(pageable, search);

            if (allMovies.isEmpty()) {
                return ResponseEntity.ok(EMPTY_LIST);
            } else {
                return ResponseEntity.ok(allMovies);
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @Override
    public HttpEntity<?> getMovieById(UUID id) {
        Optional<SingleMovieView> movieById = movieRepo.getSingleMovie(id);
        if (movieById.isPresent()) {
            return ResponseEntity.ok(movieById.get());
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }

    @Override
    public HttpEntity<?> saveMovie(UUID id, MultipartFile file, MovieDto dto) {

        LocalDate realiseDate = LocalDate.parse(dto.getRealiseDate());

        Optional<Distributor> optionalDistributor = distributorRepo.findById(dto.getDistributorId());

        List<Cast> castList = castRepo.findAllById(dto.getCastsId());

        List<Genre> genreList = genreRepo.findAllById(dto.getGenresId());

        List<Country> countryList = countryRepo.findAllById(dto.getCountriesId());

        if (optionalDistributor.isPresent()
                && !castList.isEmpty()
                && !genreList.isEmpty()
                && !countryList.isEmpty()
        ) {
            if (id != null) {
                Optional<Movie> optionalMovie = movieRepo.findById(id);

                if (optionalMovie.isPresent()) {
                    Movie movie = optionalMovie.get();

                    if (!file.isEmpty()) {
                        try {
                            Attachment attachment = Attachment.prepareAttachment(file);

                            prepareMovieToEdit(dto, optionalDistributor, castList, genreList, countryList, movie);
                            movie.setPosterImg(attachment);

                            movieRepo.save(movie);
                            return ResponseEntity.ok(SUCCESS_SAVE);
                        } catch (IOException e) {
                            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
                        }
                    } else {
                        prepareMovieToEdit(dto, optionalDistributor, castList, genreList, countryList, movie);
                        movieRepo.save(movie);
                        return ResponseEntity.ok(SUCCESS_SAVE);
                    }
                } else
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
            } else {
                try {
                    Attachment attachment = Attachment.prepareAttachment(file);

                    Movie movie = new Movie(dto.getTitle(),
                            dto.getDescription(),
                            dto.getDurationInMinute(),
                            dto.getTicketInitPrice(),
                            dto.getTrailerVideoUrl(),
                            attachment,
                            realiseDate,
                            dto.getBudget(),
                            dto.getDistributorShareInPercentage(),
                            optionalDistributor.get(),
                            castList,
                            genreList,
                            countryList
                    );

                    movieRepo.save(movie);
                    return ResponseEntity.ok(SUCCESS_SAVE);
                } catch (IOException e) {
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
                }
            }
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }

    private void prepareMovieToEdit(MovieDto dto, Optional<Distributor> optionalDistributor, List<Cast> castList, List<Genre> genreList, List<Country> countryList, Movie movie) {
        movie.setTitle(dto.getTitle());
        movie.setDescription(dto.getDescription());
        movie.setDurationInMinute(dto.getDurationInMinute());
        movie.setTicketInitPrice(dto.getTicketInitPrice());
        movie.setTrailerVideoUrl(dto.getTrailerVideoUrl());
        movie.setReleaseDate(LocalDate.parse(dto.getRealiseDate()));
        movie.setBudget(dto.getBudget());
        movie.setDistributorShareInPercent(dto.getDistributorShareInPercentage());
        movie.setDistributor(optionalDistributor.get());
        movie.setCasts(castList);
        movie.setGenres(genreList);
        movie.setCountries(countryList);
    }

    @Override
    public HttpEntity<?> deleteMovie(UUID id) {
        Optional<Movie> optionalMovie = movieRepo.findById(id);
        if (optionalMovie.isPresent()) {
            movieRepo.delete(optionalMovie.get());
            return ResponseEntity.ok(SUCCESS_DELETE);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }
}
