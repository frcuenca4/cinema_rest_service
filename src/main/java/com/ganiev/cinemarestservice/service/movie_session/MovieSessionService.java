package com.ganiev.cinemarestservice.service.movie_session;

import com.ganiev.cinemarestservice.dto.movie_session.MovieSessionDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface MovieSessionService {

    HttpEntity<?> addNewSession(MovieSessionDto dto);

    HttpEntity<?> getAllMovieSessions();

    HttpEntity<?> deleteSession(UUID id);

    HttpEntity<?> getAnnouncementSessions(UUID announcementId);
}
