package com.ganiev.cinemarestservice.service.ticket;

import com.ganiev.cinemarestservice.dto.ticket.TicketDto;
import org.springframework.http.HttpEntity;

public interface TicketService {
    HttpEntity<?> generateNewTicket(TicketDto dto);
}
