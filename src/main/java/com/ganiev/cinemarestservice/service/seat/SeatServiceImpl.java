package com.ganiev.cinemarestservice.service.seat;

import com.ganiev.cinemarestservice.entity.MovieSession;
import com.ganiev.cinemarestservice.projection.seat.AvailableSeatView;
import com.ganiev.cinemarestservice.repository.MovieSessionRepository;
import com.ganiev.cinemarestservice.repository.SeatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.OBJECT_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class SeatServiceImpl implements SeatService {

    private final SeatRepository seatRepo;
    private final MovieSessionRepository movieSessionRepo;

    @Override
    public HttpEntity<?> getAvailableSeats(UUID sessionId) {

        Optional<MovieSession> optionalSession = movieSessionRepo.findById(sessionId);

        if (optionalSession.isPresent()) {
            List<AvailableSeatView> availableSeats = seatRepo.getAvailableSeats(optionalSession.get().getId());
            return ResponseEntity.ok(availableSeats);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }
}
