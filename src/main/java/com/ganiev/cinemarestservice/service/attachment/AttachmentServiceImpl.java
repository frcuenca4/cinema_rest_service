package com.ganiev.cinemarestservice.service.attachment;

import com.ganiev.cinemarestservice.entity.Attachment;
import com.ganiev.cinemarestservice.entity.AttachmentContent;
import com.ganiev.cinemarestservice.repository.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
@Transactional
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepo;

    @Autowired
    public AttachmentServiceImpl(AttachmentRepository attachmentRepo) {
        this.attachmentRepo = attachmentRepo;
    }

    @Override
    public HttpEntity<?> saveAttachment(MultipartFile file) {
        try {
            attachmentRepo.save(Attachment.prepareAttachment(file));
            return ResponseEntity.ok(SUCCESS_SAVE);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(FAILED_TO_SAVE);
        }

    }

    @Override
    public HttpEntity<?> delete(UUID id) {

        Optional<Attachment> content = attachmentRepo.findById(id);

        if (content.isPresent()) {
            attachmentRepo.delete(content.get());
            return ResponseEntity.ok(SUCCESS_DELETE);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(FAILED_TO_DELETE);
    }

    @Override
    public HttpEntity<?> getAttachmentById(UUID id) {

        Optional<Attachment> optional = attachmentRepo.findById(id);

        if (optional.isPresent()) {

            AttachmentContent content = optional.get().getAttachmentContent();

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(optional.get().getContentType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + optional.get().getFileName() + "\"")
                    .body(new ByteArrayResource(content.getData()));
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
        }
    }

}
