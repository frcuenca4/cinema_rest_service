package com.ganiev.cinemarestservice.service.hall;

import com.ganiev.cinemarestservice.dto.hall.HallDto;
import com.ganiev.cinemarestservice.entity.Hall;
import com.ganiev.cinemarestservice.entity.Row;
import com.ganiev.cinemarestservice.repository.HallRepository;
import com.ganiev.cinemarestservice.repository.RowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
public class HallServiceImpl implements HallService {

    private final HallRepository hallRepo;
    private final RowRepository rowRepo;

    @Autowired
    public HallServiceImpl(HallRepository hallRepo, RowRepository rowRepo) {
        this.hallRepo = hallRepo;
        this.rowRepo = rowRepo;
    }

    @Override
    public HttpEntity<?> saveHall(UUID id, HallDto dto) {
        if (id != null) {
            Optional<Hall> optionalHall = hallRepo.findById(id);

            if (optionalHall.isPresent()) {
                Hall hall = optionalHall.get();
                hall.setName(dto.getName());
                hall.setVipAdditionalFeeInPercent(dto.getVipAdditionalFeeInPercent());

                Integer numberOfRows = dto.getNumberOfRows();
                if (numberOfRows != null) {
                    rowRepo.deleteAllById(rowRepo.findRowByHallId(id));

                    List<Row> rows = new ArrayList<>();
                    for (int i = 1; i <= numberOfRows; i++) {
                        rows.add(new Row(i));
                    }
                    hall.setRows(rows);
                }
                hallRepo.save(hall);
                return ResponseEntity.ok(SUCCESS_SAVE);
            } else
                return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
        }
        Optional<Hall> hall = hallRepo.findByName(dto.getName());
        if (hall.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(EXISTS);
        }
        if (dto.getNumberOfRows() != null) {
            List<Row> rows = new ArrayList<>();
            for (int i = 1; i <= dto.getNumberOfRows(); i++) {
                rows.add(new Row(i));
            }
            hallRepo.save(new Hall(dto.getName(), dto.getVipAdditionalFeeInPercent(), rows));
        } else
            hallRepo.save(new Hall(dto.getName(), dto.getVipAdditionalFeeInPercent()));
        return ResponseEntity.ok(SUCCESS_SAVE);
    }

    @Override
    public HttpEntity<?> deleteHall(UUID id) {
        Optional<Hall> hall = hallRepo.findById(id);
        if (hall.isPresent()) {
            hallRepo.delete(hall.get());
            return ResponseEntity.ok(SUCCESS_DELETE);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }

//
//    public Boolean isHallExists(HallDto hallDto) {
//
//        Hall hall = modelMapper.map(hallDto, Hall.class);
//
//        Optional<Hall> optional = hallRepo.findByName(hall.getName());
//
//        return optional.isPresent();
//
//    }
//
//    public Hall save(HallDto hallDto) {
//        Hall hall = modelMapper.map(hallDto, Hall.class);
//        return hallRepo.save(hall);
//    }
//
//    public List<Hall> getAllHalls() {
//        return hallRepo.findAll();
//    }
//
//    public Hall getHall(UUID id) {
//        Optional<Hall> hall = hallRepo.findById(id);
//        return hall.orElse(null);
//    }
//
//    public Hall editHall(UUID id, HallDto hallDto) {
//        Optional<Hall> hall = hallRepo.findById(id);
//
//        if (hall.isPresent()) {
//            hall.get().setName(hallDto.getName());
//            hall.get().setVipAdditionalFeeInPercent(hallDto.getVipAdditionalFeeInPercent());
//            hallRepo.save(hall.get());
//            return hall.get();
//        } else
//            return null;
//    }
//
//    public Boolean deleteHall(UUID id) {
//        Optional<Hall> hall = hallRepo.findById(id);
//        if (hall.isPresent()) {
//            hallRepo.delete(hall.get());
//            return true;
//        } else
//            return false;
//    }
//
//    public HttpEntity<?> getHallRows() {
//
//        return null;
//    }


}
