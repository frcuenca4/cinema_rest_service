package com.ganiev.cinemarestservice.service.hall;

import com.ganiev.cinemarestservice.dto.hall.HallDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface HallService {
    public HttpEntity<?> saveHall(UUID id, HallDto dto);

    public HttpEntity<?> deleteHall(UUID id);
}
