package com.ganiev.cinemarestservice.service.row;

import com.ganiev.cinemarestservice.dto.row.RowDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface RowService {
    HttpEntity<?> saveRowWithSeats();

    HttpEntity<?> deleteRow(UUID id);

    HttpEntity<?> saveRowSeats(RowDto dto);
}
