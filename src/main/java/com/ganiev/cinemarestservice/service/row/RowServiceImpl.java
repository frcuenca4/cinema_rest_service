package com.ganiev.cinemarestservice.service.row;

import com.ganiev.cinemarestservice.dto.row.RowDto;
import com.ganiev.cinemarestservice.entity.PriceCategory;
import com.ganiev.cinemarestservice.entity.Row;
import com.ganiev.cinemarestservice.entity.Seat;
import com.ganiev.cinemarestservice.repository.PriceCategoryRepository;
import com.ganiev.cinemarestservice.repository.RowRepository;
import com.ganiev.cinemarestservice.repository.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
public class RowServiceImpl implements RowService {

    private final RowRepository rowRepo;
    private final PriceCategoryRepository priceCatRepo;
    private final SeatRepository seatRepo;

    @Autowired
    public RowServiceImpl(RowRepository rowRepo, PriceCategoryRepository priceCategoryRepo, SeatRepository seatRepo) {
        this.rowRepo = rowRepo;
        this.priceCatRepo = priceCategoryRepo;
        this.seatRepo = seatRepo;
    }

    @Override
    public HttpEntity<?> saveRowWithSeats() {
        return null;
    }

    @Override
    public HttpEntity<?> deleteRow(UUID id) {
        Optional<Row> optionalRow = rowRepo.findById(id);
        if (optionalRow.isPresent()) {
            rowRepo.delete(optionalRow.get());
            return ResponseEntity.ok(SUCCESS_DELETE);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }

    @Override
    public HttpEntity<?> saveRowSeats(RowDto dto) {
        Optional<Row> optionalRow = rowRepo.findById(dto.getId());
        if (optionalRow.isPresent()) {
            Row row = optionalRow.get();

            if (!row.getSeats().isEmpty()) {

                // TODO: 3/19/2022 Rowni edit qilganda yangi seat qushib eskilarini uchirmayapti
                seatRepo.findSeatByRowId(row.getId());

            }

            Integer from = dto.getSeatNumFrom();
            Integer to = dto.getSeatNumTo();

            Optional<PriceCategory> optCat = priceCatRepo.findById(dto.getPriceCategoryId());

            if (optCat.isPresent()) {
                ArrayList<Seat> seats = new ArrayList<>();

                PriceCategory priceCategory = optCat.get();
                for (int i = from; i <= to; i++) {
                    seats.add(new Seat(i, priceCategory));
                }
                row.setSeats(seats);
                rowRepo.save(row);

                return ResponseEntity.ok(SUCCESS_SAVE);
            } else
                return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }
}
