package com.ganiev.cinemarestservice.service.distributor;

import com.ganiev.cinemarestservice.dto.distributor.DistributorDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface DistributorService {

    public HttpEntity<?> createNewDistributor(DistributorDto distributorDto);

    public HttpEntity<?> editDistributor(UUID id, DistributorDto dto);

    public HttpEntity<?> getDistributorById(UUID id);

    public HttpEntity<?> deleteDistributor(UUID id);

}
