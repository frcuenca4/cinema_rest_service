package com.ganiev.cinemarestservice.service.distributor;

import com.ganiev.cinemarestservice.dto.distributor.DistributorDto;
import com.ganiev.cinemarestservice.entity.Attachment;
import com.ganiev.cinemarestservice.entity.Distributor;
import com.ganiev.cinemarestservice.projection.distributor.DistributorView;
import com.ganiev.cinemarestservice.repository.DistributorRepository;
import com.ganiev.cinemarestservice.service.attachment.AttachmentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
@Transactional
public class DistributorServiceImpl implements DistributorService {

    private final DistributorRepository distributorRepository;
    private final AttachmentServiceImpl attachmentService;

    @Autowired
    public DistributorServiceImpl(DistributorRepository distributorRepository, AttachmentServiceImpl attachmentService) {
        this.distributorRepository = distributorRepository;
        this.attachmentService = attachmentService;
    }

    @Override
    public HttpEntity<?> createNewDistributor(DistributorDto dto) {
        try {
            Attachment attachment = Attachment.prepareAttachment(dto.getLogo());
            distributorRepository.save(
                    new Distributor(
                            dto.getName(),
                            dto.getDescription(),
                            attachment
                    )
            );
            return ResponseEntity.ok(SUCCESS_SAVE);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
        }
    }

    @Override
    public HttpEntity<?> editDistributor(UUID id, DistributorDto dto) {
        Optional<Distributor> distributor = distributorRepository.findById(id);
        if (distributor.isPresent()) {
            Distributor target = distributor.get();
            if (dto.getLogo() != null) {
                if (target.getLogo() != null)
                    attachmentService.delete(target.getLogo().getId());
                try {
                    target.setLogo(Attachment.prepareAttachment(dto.getLogo()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            target.setName(dto.getName());
            target.setDescription(dto.getDescription());
            Distributor save = distributorRepository.save(target);
            return ResponseEntity.status(save.getId() != null ? HttpStatus.OK : HttpStatus.CONFLICT)
                    .body(save.getId() != null ? SUCCESS_SAVE : FAILED_TO_SAVE);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }

    @Override
    public HttpEntity<?> getDistributorById(UUID id) {
        DistributorView dist = distributorRepository.getDistributorById(id);
        return ResponseEntity.status(dist.getId() != null ? HttpStatus.OK : HttpStatus.CONFLICT)
                .body(dist.getId() != null ? dist : OBJECT_NOT_FOUND);
    }

    @Override
    public HttpEntity<?> deleteDistributor(UUID id) {
        distributorRepository.findById(id).ifPresent(distributorRepository::delete);
        return ResponseEntity.ok(SUCCESS_DELETE);
    }
}
