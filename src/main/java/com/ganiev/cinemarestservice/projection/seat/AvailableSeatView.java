package com.ganiev.cinemarestservice.projection.seat;

import java.util.UUID;

public interface AvailableSeatView {
    UUID getId();

    Integer getSeatNumber();

    Integer getRowNumber();

    Boolean getIsAvailable();
}
