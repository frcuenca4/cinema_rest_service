package com.ganiev.cinemarestservice.projection.movie_session;



import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDate;
import java.util.List;

public interface SessionHallDateView {

    LocalDate getDate();

    @Value("#{@movieSessionRepository.getSessionHalls(target.announcementId, target.date)}")
    List<SessionHallTimeView> getHalls();
}
