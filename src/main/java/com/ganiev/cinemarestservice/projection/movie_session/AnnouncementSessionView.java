package com.ganiev.cinemarestservice.projection.movie_session;

import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface AnnouncementSessionView {
    UUID getMovieId();

    String getMovieTitle();

    @Value("#{@movieSessionRepository.getSessionDates({target.announcementId})}")
    List<SessionHallDateView> getDates();

}
