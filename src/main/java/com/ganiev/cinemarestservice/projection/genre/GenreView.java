package com.ganiev.cinemarestservice.projection.genre;

import java.util.UUID;

public interface GenreView {
    UUID getId();

    String getName();
}
