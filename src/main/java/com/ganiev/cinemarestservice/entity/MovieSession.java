package com.ganiev.cinemarestservice.entity;

import com.ganiev.cinemarestservice.entity.template.AbsEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.time.LocalTime;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class MovieSession extends AbsEntity {

    @ManyToOne
    MovieAnnouncement movieAnnouncement;

    @ManyToOne
    Hall hall;

    LocalDate date;

    LocalTime startTime;

    LocalTime endTime;

    Double additionalFee;
}

