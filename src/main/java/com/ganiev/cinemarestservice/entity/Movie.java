package com.ganiev.cinemarestservice.entity;

import com.ganiev.cinemarestservice.entity.template.AbsEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Movie extends AbsEntity {

    @Column(nullable = false)
    String title;

    @Column(nullable = false, columnDefinition = "TEXT")
    String description;

    @Column(nullable = false)
    Integer durationInMinute;

    @Column(nullable = false)
    Double ticketInitPrice;

    @Column(nullable = false)
    String trailerVideoUrl;

    @OneToOne(cascade = CascadeType.ALL)
    Attachment posterImg;

    @Column(nullable = false)
    LocalDate releaseDate;

    @Column(nullable = false)
    Double budget;

    @Column(nullable = false)
    Double distributorShareInPercent;

    @ManyToOne(cascade = CascadeType.ALL)
    Distributor distributor;

    @ManyToMany(cascade = CascadeType.ALL)
    List<Cast> casts;

    @ManyToMany(cascade = CascadeType.ALL)
    List<Genre> genres;

    @ManyToMany(cascade = CascadeType.ALL)
    List<Country> countries;


}
