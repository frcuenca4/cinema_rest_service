package com.ganiev.cinemarestservice.entity;

import com.ganiev.cinemarestservice.entity.enums.TicketStatus;
import com.ganiev.cinemarestservice.entity.template.AbsEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.mapping.ToOne;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Ticket extends AbsEntity {

    @ManyToOne
    MovieSession movieSession;

    @OneToOne
    Seat seat;

    @OneToOne
    Attachment qrCode;

    @Column(nullable = false)
    Double price;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    TicketStatus status;

    @OneToOne
    User user;
}
    