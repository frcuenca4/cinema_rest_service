package com.ganiev.cinemarestservice.entity;

import com.ganiev.cinemarestservice.entity.enums.CastType;
import com.ganiev.cinemarestservice.entity.template.AbsEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity(name = "casts")
public class Cast extends AbsEntity {

    @Column(nullable = false)
    String fullName;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    CastType castType;


    @OneToOne(cascade = CascadeType.ALL)
    Attachment photo;


}
