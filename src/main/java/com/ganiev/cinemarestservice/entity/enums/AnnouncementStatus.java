package com.ganiev.cinemarestservice.entity.enums;

public enum AnnouncementStatus {
    ACTIVE,
    INACTIVE,
    SOON

}
