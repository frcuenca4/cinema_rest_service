package com.ganiev.cinemarestservice.entity.enums;

public enum CastType {
    CAST_ACTOR,
    CAST_ACTRESS,
    CAST_DIRECTOR
}
